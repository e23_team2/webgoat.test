﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebGoat.NET.ModelsPrimitives;
using WebGoatCore.Models;

namespace WebGoat.Test
{
    [TestClass]
    public class BlogResponseTest
    {
        [TestMethod]
        [DataRow("hejsa!")]
        [DataRow("<script>alert('hacked!');</script>")]
        public void IsContentValid_Allowed(string contents)
        {
            var contentsP = new ContentsPrimitive(contents);
            Assert.AreEqual(HttpUtility.HtmlEncode(contents), contentsP.CleanContents);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("257_length_input_qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq_Continues_off_screen...qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq")]
        public void IsContentValid_ThrowException(string contents) 
        {
            Assert.ThrowsException<ArgumentException>(() => new ContentsPrimitive(contents));
        }

        [TestMethod]
        [DataRow("Anonymous")]
        [DataRow("Testn97!")]
        [DataRow("Y.E.P")]
        public void IsAuthorValid_Allowed(string author)
        {
            var authorP = new AuthorPrimitive(author);
            Assert.AreEqual(author, authorP.CleanAuthor);
        }

        [TestMethod]
        [DataRow("ÆØÅ")]
        [DataRow("N%O;P?E")]
        [DataRow("257_length_input_qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq")]
        public void IsAuthorValid_ThrowException(string author)
        {
            Assert.ThrowsException<ArgumentException>(() => new AuthorPrimitive(author));
        }

        [TestMethod]
        [DataRow(0)]
        [DataRow(Int32.MaxValue)]
        public void IsBlogEntryIdValid_Allowed(int blogEntryId)
        {
            var blogEntryIdP = new BlogEntryIdPrimitive(blogEntryId);
            Assert.AreEqual(blogEntryId, blogEntryIdP.CleanBlogEntryId);
        }

        [TestMethod]
        [DataRow(-1)]
        public void IsBlogEntryIdValid_ThrowException_Min(int blogEntryId)
        {
            Assert.ThrowsException<ArgumentException>(() => new BlogEntryIdPrimitive(blogEntryId));
        }
    }
}
