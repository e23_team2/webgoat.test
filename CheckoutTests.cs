using WebGoatCore.Models;

namespace WebGoat.Test
{
    [TestClass]
    public class CheckoutTests
    {
        [TestMethod]
        [DataRow("Denmark")] //Arrange
        [DataRow("France")]
        [DataRow("Russia")]
        public void IsCountryValid_Allowed(string country)
        {
            //Act
            var countryP = new CountryPrimitive(country);
            //Assert 
            Assert.AreEqual(country, countryP.CleanCountry);
        }

        [TestMethod]
        [DataRow("');DELETE FROM BlogResponses;--")] //Arrange
        [DataRow("Disneyland")]
        [DataRow("12312!!!2�.,,?HEJ")]
        [DataRow("Frankrig")]
        [DataRow(null)]
        public void IsCountryValid_ThrowException(string country)
        {
            Assert.ThrowsException<ArgumentException>(() => new CountryPrimitive(country));
        }

        [TestMethod]
        [DataRow("1234")] //Arrange
        [DataRow("12345")]
        [DataRow("123456")]
        [DataRow("5000")]
        public void IsPostalCodeValid_Allowed(string postalCode) 
        {
            //Act
            var postalCodeP = new PostalCodePrimitive(postalCode);

            //Assert
            Assert.AreEqual(postalCode, postalCodeP.PostalCode);
        }

        [TestMethod]
        [DataRow("123")] //Arrange
        [DataRow("1234567")]
        [DataRow("JegHackerDig;")]
        [DataRow("`?!''''%&�$@;''-_")]
        [DataRow(null)]
        public void IsPostalCodeValid_ThrowException(string postalCode)
        {
            Assert.ThrowsException<ArgumentException>(() => new PostalCodePrimitive(postalCode));
        }

        [TestMethod]
        [DataRow(1)]
        public void IsShippingMethodValid_Allowed(int shippingMethod)
        {
            var shippingMethodP = new ShippingMethodPrimitive(shippingMethod);
            Assert.AreEqual(shippingMethod, shippingMethodP.CleanShippingMethod);
        }

        [TestMethod]
        [DataRow(-1)]
        [DataRow(8)]
        public void IsShippingMethodValid_ThrowException(int shippingMethod)
        {
            Assert.ThrowsException<ArgumentException>(() => new ShippingMethodPrimitive(shippingMethod));
        }

        [TestMethod]
        [DataRow("4111111111111111")]
        [DataRow("4111 1111 1111 1111")]
        public void IsCreditCardNumberValid_Allowed(string creditCardNumber)
        {
            var creditCardNumberP = new CreditCardNumberPrimitive(creditCardNumber);
            Assert.AreEqual(creditCardNumber, creditCardNumberP.CleanCreditCardNumber);
        }

        [TestMethod]
        [DataRow("hacked???")]
        [DataRow("1234")]
        [DataRow("4111111111111119")]
        public void IsCreditCardNumberValid_ThrowException(string creditCardNumber)
        {
            Assert.ThrowsException<ArgumentException>(() => new CreditCardNumberPrimitive(creditCardNumber));
        }

        [TestMethod]
        [DataRow(01)]
        [DataRow(12)]
        public void IsCreditCardExpMonthValid_Allowed(int creditCardExpMonth)
        {
            var creditCardExpMonthP = new CreditCardExpMonthPrimitive(creditCardExpMonth);
            Assert.AreEqual(creditCardExpMonth, creditCardExpMonthP.CleanCreditCardExpMonth);
        }

        [TestMethod]
        [DataRow(0)]
        [DataRow(13)]
        public void IsCreditCardExpMonthValid_ThrowException(int creditCardExpMonth)
        {
            Assert.ThrowsException<ArgumentException>(() => new CreditCardExpMonthPrimitive(creditCardExpMonth));
        }

        [TestMethod]
        [DataRow(2023)]
        [DataRow(2032)]
        public void IsCreditCardExpYearValid_Allowed(int creditCardExpYear)
        {
            var creditCardExpYearP = new CreditCardExpYearPrimitive(creditCardExpYear);
            Assert.AreEqual(creditCardExpYear, creditCardExpYearP.CleanCreditCardYear);
        }

        [TestMethod]
        [DataRow(2022)]
        [DataRow(2050)]
        public void IsCreditCardExpYearValid_ThrowException(int creditCardExpYear)
        {
            Assert.ThrowsException<ArgumentException>(() => new CreditCardExpYearPrimitive(creditCardExpYear));
        }

        [TestMethod]
        [DataRow("4111 1111 1111 1111", 01, 2024)]
        [DataRow("4111 1111 1111 1111", 12, 2030)]
        public void IsCreditCardDateValid_Allowed(string number, int month, int year)
        {
            var date = new CleanCreditCard(new CreditCardNumberPrimitive(number), new CreditCardExpMonthPrimitive(month), new CreditCardExpYearPrimitive(year));
            Assert.AreEqual(new DateTime(year, month, 1), date.CleanExpireDate);
        }

        [TestMethod]
        [DataRow("4111 1111 1111 1111", 01, 2023)]
        [DataRow("4111 1111 1111 1111", 12, 2050)]
        [DataRow("4111 1111 1111 1111", 00, 2024)]
        [DataRow("4111 1111 1111 1111", null, null)]
        public void IsCreditCardDateValid_ThrowException(string number, int month, int year)
        {
            Assert.ThrowsException<ArgumentException>(() => new CleanCreditCard(new CreditCardNumberPrimitive(number), new CreditCardExpMonthPrimitive(month), new CreditCardExpYearPrimitive(year)));
        }

        [TestMethod]
        public void IsCityValid_Allowed(string city)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsCityValid_ThrowException(string city)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsAddressValid_Allowed(string address)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsAddressValid_ThrowException(string address)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsRegionValid_Allowed(string region)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsRegionValid_ThrowException(string region)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsShipTargetNameValid_Allowed(string shipTargetName)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void IsShipTargetNameValid_ThrowException(string shipTargetName)
        {
            throw new NotImplementedException();
        }
    }
}